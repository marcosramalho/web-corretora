<div class="slider">
  <ul class="slides">
    <li>
      <img src="{{ asset('img/modelo_slide_1.jpg')}}" alt="Imagem">
      <div class="caption center-align">
        <h3>Titulo da imagem</h3>
        <h5>Descrição do SLide</h5>
      </div>
    </li>
    <li>
      <img src="{{ asset('img/modelo_slide_2.jpg')}}" alt="Imagem">
      <div class="caption center-align">
        <h3>Titulo da imagem</h3>
        <p>Descrição do SLide</p>
      </div>
    </li>
    <li>
      <img src="{{ asset('img/modelo_slide_3.jpg')}}" alt="Imagem">
      <div class="caption center-align">
        <h3>Titulo da imagem</h3>
        <h5>Descrição do SLide</h5>
      </div>
    </li>
  </ul>
</div>