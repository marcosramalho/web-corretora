@extends('layouts.site')

@section('content')

<div class="container">
  <div class="row section">
    <h3 class="center-align">Contato</h3>
    <div class="divider"></div>
  </div>
  <div class="row section">
    <div class="col s12 m7">
      <img class="responsive-img" src="{{ asset('img/modelo_img_home.jpg') }}" alt="">
    </div>
    <div class="col s12 m5">
      <form class="col s12">
        <div class="input-field">
          <input type="text" name="nome" id="nome" class="validate">
          <label for="nome">Nome</label>
        </div>
        <div class="input-field">
          <input type="text" name="email" id="email" class="validate">
          <label for="email">E-mail</label>
        </div>
        <div class="input-field">
          <textarea class="materialize-textarea" id="mensagem"></textarea>
          <label for="mensagem">Mensagem</label>
        </div>
        <button class="btn button-blue">Enviar</button>
      </form>
    </div>
  </div>
</div>
@endsection