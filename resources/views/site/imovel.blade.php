@extends('layouts.site')

@section('content')

<div class="container">
  <div class="row section">
    <h3 class="center-align">Imóvel</h3>
    <div class="divider"></div>
  </div>
  <div class="row section">
    <div class="col s12 m8">
      <div class="row">
        <div class="slider">
          <ul class="slides">
            <li>
              <img src="{{ asset('img/modelo_detalhe_1.jpg') }}" alt="Detalhe 1">
              <div class="caption center-align">
                <h3>Titulo da image</h3>
                <h5>Descrição da imagem</h5>
              </div>
            </li>
            <li>
              <img src="{{ asset('img/modelo_detalhe_2.jpg') }}" alt="Detalhe 2">
              <div class="caption left-align">
                <h3>Titulo da image</h3>
                <h5>Descrição da imagem</h5>
              </div>
            </li>
            <li>
              <img src="{{ asset('img/modelo_detalhe_3.jpg') }}" alt="Detalhe 3">
              <div class="caption right-align">
                <h3>Titulo da image</h3>
                <h5>Descrição da imagem</h5>
              </div>
            </li>
            <li>
              <img src="{{ asset('img/modelo_detalhe_4.jpg') }}" alt="Detalhe 4">
              <div class="caption left-align">
                <h3>Titulo da image</h3>
                <h5>Descrição da imagem</h5>
              </div>
            </li>
            
          </ul>
        </div>
      </div>
      <div class="row" align="center">
        <button onclick="sliderPrev()" class="btn blue">Anterior</button>
        <button onclick="sliderNext()" class="btn blue">Próxima</button>
      </div>
    </div>
    <div class="col s12 m4">
      <h4>Titulo do Imóvel</h4>
      <blockquote>
        Descrição breve sobre o imóvel
      </blockquote>
      <p><b>Código:</b> 245 </p>
      <p><b>Status:</b> Vende </p>
      <p><b>Tipo:</b> Alvenaria </p>
      <p><b>Endereço:</b> Centro </p>
      <p><b>Cep:</b> 69000-000 </p>
      <p><b>Cidade:</b> Manaus </p>
      <p><b>Estado:</b> Amazonas </p>
      <p><b>Valor:</b> R$ 200.000,00 </p>
      <a class="btn deep-orange darken-1" href="{{ route('site.contato') }}">Entrar em contato</a>
    </div>
  </div>
</div>
@endsection