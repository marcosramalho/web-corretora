<?php

use Illuminate\Database\Seeder;
use App\Pagina;

class PaginasSeeds extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $existe = Pagina::where('tipo', '=', 'sobre')->count();

        //dd($existe); imprime e para a aplicação

        if ($existe) {
            $paginaSobre = Pagina::where('tipo', '=', 'sobre')->first();

        }  else {
            $paginaSobre = new Pagina();
        }

        $paginaSobre->titulo = "Titulo da empresa";
        $paginaSobre->descricao = " Descrição breve sobre a empresa.";
        $paginaSobre->texto = "Texto sobre a empresa.";
        $paginaSobre->imagem = "img/modelo_img_home.jpg";
        $paginaSobre->mapa = '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3760.4796642660735!2d-59.9274883503531!3d-3.070772138128843!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x493631de4f5704cf!2sBarat%C3%A3o+da+Carne+-+Grande+Vitoria!5e0!3m2!1spt-BR!2sbr!4v1526938064107" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>';
        $paginaSobre->tipo = "sobre";
        $paginaSobre->save();
        echo "Página sobre criada com sucesso!\n";
    }
}
