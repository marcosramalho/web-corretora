<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
Use App\User;

class UsuarioController extends Controller
{
    public function login(Request $request) {
        $dados = $request->all();
        //dd($dados);

        if (Auth::attempt(['email'=>$dados['email'], 'password'=>$dados['password']])) {
            \Session::flash('mensagem', ['msg'=>"Login realizado com sucesso!", 'class'=> 'green white-text']);

            return redirect()->route('admin.principal');
        }

        \Session::flash('mensagem', ['msg'=>"Error, Email/senha incorretos!", 'class'=> 'red white-text']);
        return redirect()->route('admin.login');

    }

    public function logout() {
        Auth::logout();
        return redirect()->route('admin.login');
    }

    public function index() {
        $usuarios = User::all();
        
        return view('admin.usuarios.index', compact('usuarios'));
    }

    public function adicionar() {
        return view('admin.usuarios.adicionar');
    }

    public function salvar(Request $request) {
        $data = $request->all();
        
        $usuario = new User();
        $usuario->name = $data['name'];
        $usuario->email = $data['email'];
        $usuario->password = bcrypt($data['password']);
        $usuario->save();

        \Session::flash('mensagem', ['msg'=>"Registro salvo com sucesso!", 'class'=> 'green white-text']);

        return redirect()->route('admin.usuarios');        
    }

    public function editar($id) {

        $usuario = User::find($id);

        return view('admin.usuarios.editar', compact('usuario'));
    }

    public function atualizar(Request $request, $id) {
        $usuario = User::find($id);

        $data = $request->all();

        if (isset($data['password']) && strlen($data['password']) > 5) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }

        $usuario->update($data);

        \Session::flash('mensagem', ['msg'=>"Registro atualizado com sucesso!", 'class'=> 'green white-text']);

        return redirect()->route('admin.usuarios');        
    }

    public function deletar($id) {
        User::find($id)->delete();
        
        \Session::flash('mensagem', ['msg'=>"Registro deletado com sucesso!", 'class'=> 'green white-text']);

        return redirect()->route('admin.usuarios');        
    }
}
